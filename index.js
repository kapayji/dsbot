import Discord from "discord.js";
import fs from "fs";
import config from "config";
import fetch from "node-fetch";

const client = new Discord.Client();

client.commands = new Discord.Collection();

fs.readdir("./commands", (err, files) => {
  if (err) {
    console.log(err);
  }
  let jsfile = files.filter((f) => f.split(".").pop() === "js");
  if (jsfile.length <= 0) {
    return console.log("Команды не найдены");
  }
  console.log(`Загружено ${jsfile.length} команд`);
  jsfile.forEach(async (f, i) => {
    await import(`./commands/${f}`).then((expCom) => {
      client.commands.set(expCom.help.name, expCom);
      console.log(client.commands);
    });
  });
});
client.on("ready", () => {
  console.log(`Бот ${client.user.username} запустился`);
});

client.on("message", (message) => {
  let prefix = config.prefix;
  if (!message.content.startsWith(prefix) || message.author.bot) {
    return;
  }
  let messageArray = message.content.split("  ");
  let command = messageArray[0];
  let args = messageArray.slice(1);
  let command_file = client.commands.get(command.slice(prefix.length));
  if (command_file) {
    command_file.run(client, message, args, prefix);
  }
});

client.login(process.env.BOT_TOKEN);
