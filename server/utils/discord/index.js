import fetch from "node-fetch";

export let sendInDiscord = async (channel, body) => {
  await fetch(
    //channel лог-системный в ДС ВША
    `https://discordapp.com/api/v9/channels/${channel}/messages`,
    {
      method: "POST",
      headers: {
        "Content-Disposition": 'form-data; name="payload_json"',
        "Content-Type": "application/json",
        Authorization:
          "Bot OTAyODQwMTYyMTkzNjM3NDQ3.YXkRbA.jbbt-nltGNZxmSxDJrePhte3_f4",
      },
      body: JSON.stringify(body),
    }
  );
};
