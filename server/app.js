import express from "express";
import mongoose from "mongoose";
import fs from "fs";
import cors from "cors";
import config from "config";
import path from "path";
const __dirname = path.resolve();
const app = express();
const PORT = config.get("port") || 5000;
const MONGO_DB_URL = config.get("mongoDbUrl");

import { messagesRouter } from "./routes/messages/mesaages.routes.js";
import { restartRouter } from "./routes/restart/restart.routes.js";
import { queryRouter } from "./routes/querys/querys.routes.js";
import { testRouter } from "./routes/testRoute/test.routes.js";
import { playersRouter } from "./routes/players/players.routes.js";
import { chatRouter } from "./routes/chat/chat.routes.js";
import { systemmessageRouter } from "./routes/systemmessage/systemmessage.routes.js";
import { zeusRouter } from "./routes/zeus/zeus.routes.js";
import { userRoutes } from "./routes/users/users.routes.js";
import { dsUserRoutes } from "./routes/dsuser/dsuser.routes.js";

app.use(cors());
app.use(express.json({ extended: true }));
app.use(express.static("public"));
app.use("/api/mes", messagesRouter);
app.use("/api/restart", restartRouter);
app.use("/api/players", queryRouter);
app.use("/api/testcommands", testRouter);
app.use("/api/players", playersRouter);
app.use("/api/chat", chatRouter);
app.use("/api/systemmessage", systemmessageRouter);
app.use("/api/zeus", zeusRouter);
app.use("/api/user", userRoutes);
app.use("/api/dsuser/", dsUserRoutes);

app.get("/:name/:position/:uid", async (req, res) => {
  console.log(123);
  const name = req.params.name;
  const position = req.params.position;
  const uid = req.params.uid;

  const prefix =
    "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/><title>PageTitle</title></head><body>";
  const suffix = "</body></html>";
  const result =
    prefix +
    `<p align="center">Hello1</p><br>` +
    `<p align="center">${name}</p>` +
    `<p align="right">${position}</p>` +
    `<p align="right">${uid}</p><br>` +
    `<p align="center"><img src="https://cdnn21.img.ria.ru/images/148839/96/1488399659_0:0:960:960_600x0_80_0_1_e38b72053fffa5d3d7e82d2fe116f0b3.jpg" width="200" height="200"/></p>` +
    suffix;
  await fs.writeFileSync(`./public/test/index.html`, result, {
    encoding: "utf8",
    flag: "w",
  });
  res.status(200).sendFile(__dirname + "/public/test/index.html");
  // res.send("<p>123123123</p>");
});

async function start() {
  try {
    await mongoose.connect(MONGO_DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    app.listen(PORT, () => {
      console.log(`Server start on ${PORT}`);
    });
  } catch (e) {
    console.log(e.message);
  }
}
start();
