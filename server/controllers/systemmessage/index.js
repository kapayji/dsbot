import { sendInDiscord } from "../../utils/discord/index.js";
import config from "config";
import Player from "../../models/players/index.js";
import iconv from "iconv";
const { Iconv } = iconv;

export let handleMessage = async (req, res) => {
  try {
    const message = req.params.message;
    var iconv = new Iconv("UTF-8", "CP1251");
    var conv_message = iconv.convert(message);
    // let start_str = { prefix: "```json", suffix: "```" };
    const body = {
      content: `${conv_message.toString()}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    // console.log(JSON.stringify(result_string.toString("utf-8")));
    res.send(`['ok']`);
  } catch (e) {
    console.log(e);
  }
};
