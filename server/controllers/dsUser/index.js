import dsUser from "../../models/dsUser/index.js";
import config from "config";
import fetch from "node-fetch";
import { sendInDiscord } from "../../utils/discord/index.js";

export let addSubscribeToUser = async (req, res) => {
  try {
    const guild_id = req.params.guild_id;
    const user_id = req.params.user_id;
    const role_id = req.params.role_id;
    const delay = req.params.delay;
    const response = await fetch(
      `https://discordapp.com/api/v9/guilds/${guild_id}/members/${user_id}/roles/${role_id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "X-Audit-Log-Reason",
          Authorization:
            "Bot OTAyODQwMTYyMTkzNjM3NDQ3.YXkRbA.jbbt-nltGNZxmSxDJrePhte3_f4",
        },
      }
    );
    // console.log(response.ok);
    if (response.ok) {
      const exist = await dsUser.findOne({ user_id });
      if (exist) {
        console.log(exist.roles.findIndex((x) => x.id == role_id));
        if (exist.roles.findIndex((x) => x.id == role_id) == -1) {
          exist.roles.push({
            id: role_id,
            expiringDate: new Date(1000 * 60 * 60 * 24 * delay + Date.now()),
          });
          console.log(exist);
          await exist.save();
          let indexRoleInPlayerObj = exist.roles.findIndex(
            (x) => x.id == role_id
          );
          let body = {
            content: `<@${user_id}>`,
            embeds: [
              {
                title: "System Info",
                color: "14177041",
                fields: [
                  {
                    name: `<@${user_id}>`,
                    value: `Пользователь обновлён, роль добавлена,\nДата истечения: ${exist.roles[indexRoleInPlayerObj].expiringDate}`,
                  },
                ],

                thumbnail: {
                  url: "https://discdev.ru/images/6.png",
                },
                timestamp: new Date(Date.now()),
              },
            ],
          };
          await sendInDiscord(config.get("rolesChannel"), body);
          res.json({ message: "pushed" });
        } else {
          let indexRoleInPlayerObj = exist.roles.findIndex(
            (x) => x.id == role_id
          );
          let body = {
            content: `<@${user_id}>`,
            embeds: [
              {
                title: "System Info",
                color: "14177041",
                fields: [
                  {
                    name: `<@${user_id}>`,
                    value: `У пользователя уже есть данная роль, истекает ${exist.roles[indexRoleInPlayerObj].expiringDate}`,
                  },
                ],

                thumbnail: {
                  url: "https://discdev.ru/images/6.png",
                },
                timestamp: new Date(Date.now()),
              },
            ],
          };
          await sendInDiscord(config.get("rolesChannel"), body);
          res.json({ message: "role alredy added" });
        }
      } else {
        const userDs = new dsUser({
          guild_id,
          user_id,
          roles: {
            id: role_id,
            expiringDate: new Date(1000 * 60 * 60 * 24 * delay + Date.now()),
          },
        });
        await userDs.save();
        let indexRoleInPlayerObj = userDs.roles.findIndex(
          (x) => x.id == role_id
        );
        let body = {
          content: `<@${user_id}>`,
          embeds: [
            {
              title: "System Info",
              color: "14177041",
              fields: [
                {
                  name: `<@${user_id}>`,
                  value: `Пользователь создан, роль добавлена,\nДата истечения ${userDs.roles[indexRoleInPlayerObj].expiringDate}`,
                },
              ],

              thumbnail: {
                url: "https://discdev.ru/images/6.png",
              },
              timestamp: new Date(Date.now()),
            },
          ],
        };
        await sendInDiscord(config.get("rolesChannel"), body);
        return res.json({ message: "ok" });
      }
    }
  } catch (e) {
    console.log(e);
  }
};
export let removeSubscribeFromUser = async (req, res) => {
  try {
    const guild_id = req.params.guild_id;
    const user_id = req.params.user_id;
    const role_id = req.params.role_id;

    const response = await fetch(
      `https://discordapp.com/api/v9/guilds/${guild_id}/members/${user_id}/roles/${role_id}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "X-Audit-Log-Reason",
          Authorization:
            "Bot OTAyODQwMTYyMTkzNjM3NDQ3.YXkRbA.jbbt-nltGNZxmSxDJrePhte3_f4",
        },
      }
    );
    if (response.ok) {
    }
  } catch (e) {
    console.log(e);
  }
};
//http://localhost:5000/api/dsuser/addrole/657316140120539177/402872883371573248/908334253467656262/25

export let getUserRoles = async (req, res) => {
  try {
    const guild_id = req.params.guild_id;
    const user_id = req.params.user_id;

    const response = await fetch(
      `https://discordapp.com/api/v9/guilds/${guild_id}/roles`,
      {
        method: "GET",
        headers: {
          "Content-Type": "X-Audit-Log-Reason",
          Authorization:
            "Bot OTAyODQwMTYyMTkzNjM3NDQ3.YXkRbA.jbbt-nltGNZxmSxDJrePhte3_f4",
        },
      }
    );
    const data = await response.json();
    // console.log(data);
    if (response.ok) {
      const exist = await dsUser.findOne({ user_id });
      if (exist) {
        const rolesUser = await data.map((v) => {
          exist.roles.forEach((x) => x.id === v.id);
        });
        console.log(rolesUser);
        res.json({ message: "ok" });
      }
    }
  } catch (e) {
    console.log(e);
  }
};
