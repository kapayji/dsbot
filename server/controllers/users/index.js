import bcrypt from "bcryptjs";
import validator from "express-validator";
import User from "../../models/users/index.js";
import mailer from "../../nodemailer/index.js";

const { validationResult } = validator;

export let registrationUser = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
        message: "Incorrect registration data",
      });
    }
    const { email, password } = await req.body;
    const candidate = await User.findOne({ email });
    if (candidate) {
      return res.status(400).json({ message: "Already registered" });
    }
    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({ email, password: hashedPassword });
    console.log(user);
    await user.save();
    const message = {
      from: "ArmA 3 LFP <kapayji@gmail.com>",
      to: email,
      subject: "Регистрация на проекте LFP",
      // text: "Вы были успешно зарегистрированы на проекте LFP, в системе ArmA 3 LFP Web",
      html: "<!DOCTYPE html><html lang='en'>  <head>    <meta charset='UTF-8' />    <meta http-equiv='X-UA-Compatible' content='IE=edge' />    <meta name='viewport' content='width=device-width, initial-scale=1.0' />    <title>Document</title>  </head>  <body>    <div class='body_bkg'></div>    <div class='container'>      <div class='header__img'>        <img          src='https://discdev.ru/images/6.png'          alt=''          width='100'          height='100'        />      </div>      <h1 class='title'>LFP Arma 3</h1>      <div>        Добро пожаловать на проект LFP Arma 3        <ul>          Список чего-то          <li>1.Хуё-моё</li>          <li>2.трали-вали</li>          <li>3.сри</li>          <li>4.фор</li>          <li>5.фюнф</li>        </ul>        <hr />        <div>Футер с каким-то содержимым</div>      </div>    </div>    <style>      .header__img {        justify-content: center;      }      .title {        color: aliceblue;        background-color: aqua;      }      ul {        list-style: none;        margin: 20px 0 10px 0;        padding: 0;      }      .container {        top: 50%;        margin: auto;        border: 1px solid gray;        text-align: center;        height: auto;      }      .body_bkg {        position: absolute;        margin: 0 auto;        height: 100%;        width: 100%;        background-image: url('http://discdev.ru/4jycybwbkxz21.png');        background-size: cover;        background-position: center;        /* transform: scale(1, 1); */        filter: blur(8px) brightness(100%) contrast(100%)          drop-shadow(2px 2px 2px rgba(250, 250, 250, 1));        z-index: -1;      }    </style>  </body></html>",
    };
    mailer(message);
    res.status(201).json({ message: "User created" });
  } catch (e) {
    res.status(500).json({ message: "Registration error, try again" });
    console.log(e);
  }
};
