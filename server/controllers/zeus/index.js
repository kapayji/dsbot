import { sendInDiscord } from "../../utils/discord/index.js";
import config from "config";
import Player from "../../models/players/index.js";
import Zeus from "../../models/zeus/index.js";
import fetch from "node-fetch";
import iconv from "iconv";
const { Iconv } = iconv;

const chat_channel = [{ prefix: "```css\n", suffix: "```" }];

export let zeusGetToPlayer = async (req, res) => {
  try {
    const steam_id = req.params.steam_id;
    const role = req.params.role;
    const initial = req.params.initial;
    let body = {};
    let date = new Date(Date.now());
    console.log(`${date}`);
    const zeus_exits = await Zeus.findOne({ steam_id });
    if (zeus_exits) {
      body = {
        embeds: [
          {
            title: "Zeus Info",
            color: "14177041",
            description: `Запись с UID(${steam_id}) уже есть в БД`,
            thumbnail: {
              url: "https://discdev.ru/images/6.png",
            },
            timestamp: date,
          },
        ],
      };
      sendInDiscord(config.get("zeusLog"), body);
      return res.status(400).send("ok");
    }
    const new_zeus = new Zeus({
      steam_id,
      role,
      initial,
    });

    await new_zeus.save();
    body = {
      embeds: [
        {
          title: "Zeus Info",
          color: "14177041",
          description: `Запись UID(${steam_id}) создана в БД\nИнициатор: ${initial}`,
          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: date,
        },
      ],
    };
    sendInDiscord(config.get("zeusLog"), body);
    res.send("ok");
  } catch (e) {
    console.log(e);
  }
};

export let zeusRemoveFromPlayer = async (req, res) => {
  try {
    const steam_id = req.params.steam_id;
    const initial = req.params.initial;
    const zeus_exits = await Zeus.findOne({ steam_id });
    let body;
    let date = new Date(Date.now());
    console.log(date);
    if (!zeus_exits) {
      body = {
        embeds: [
          {
            title: "Zeus Info",
            color: "14177041",
            description: `Зевс не найден в БД`,
            thumbnail: {
              url: "https://discdev.ru/images/6.png",
            },
            timestamp: date,
          },
        ],
      };
      sendInDiscord(config.get("zeusLog"), body);
      return res.status(400).json({ message: "Зевс не найден в БД" });
    }
    await Zeus.findOneAndDelete(zeus_exits);

    body = {
      embeds: [
        {
          title: "Zeus Info",
          color: "14177041",
          description: `Запись UID(${steam_id}) удалена из БД\nИнициатор: ${initial}`,
          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: date,
        },
      ],
    };
    sendInDiscord(config.get("zeusLog"), body);
    res.json({ message: `Зевс удалён` });
  } catch (e) {
    console.log(e);
  }
};

export let curatorWatchingCreateGroup = async (req, res) => {
  try {
    const name = req.params.name;
    const steam_id = req.params.steam_id;
    const entityName = req.params.entityName;
    const position = req.params.position;
    const count = req.params.count;
    var iconv = new Iconv("UTF-8", "CP1251");
    // var result_string = iconv.convert(message);
    var entity = iconv.convert(entityName);
    // console.log(name);
    var encodeName = iconv.convert(name);
    encodeName = encodeName.toString().replace(/\[|]/g, "");
    const body = {
      content: `${
        chat_channel[0].prefix
      }Куратор [${encodeName.toString()}(${steam_id})]: создал группу ${entity.toString()}, на позиции ${position}, численность ${count}${
        chat_channel[0].suffix
      }`,
    };
    sendInDiscord(config.get("zeusLog"), body);
    // console.log(JSON.stringify(result_string.toString("utf-8")));
    res.send(`['ok']`);
  } catch (e) {
    console.log(e);
  }
};
export let curatorWatchingCreateObject = async (req, res) => {
  try {
    const name = req.params.name;
    const steam_id = req.params.steam_id;
    const entityName = req.params.entityName;
    const position = req.params.position;
    console.log(entityName);
    var iconv = new Iconv("UTF-8", "CP1251");
    // var result_string = iconv.convert(message);
    var entity = iconv.convert(entityName);
    //console.log(name);
    var encodeName = iconv.convert(name);
    encodeName = encodeName.toString().replace(/\[|]/g, "");
    // console.log(
    //   `${chat_channel[channel].prefix}[${name}]: ${result_string.toString()}\n${
    //     chat_channel[channel].suffix
    //   }`
    // );
    // let start_str = { prefix: "```json", suffix: "```" };
    const body = {
      content: `${
        chat_channel[0].prefix
      }Куратор [${encodeName}(${steam_id})]: создал ${entity.toString()}, на позиции ${position}${
        chat_channel[0].suffix
      }`,
    };
    sendInDiscord(config.get("zeusLog"), body);
    // console.log(JSON.stringify(result_string.toString("utf-8")));
    res.send(`['ok']`);
  } catch (e) {
    console.log(e);
  }
};
export let curatorWatchingDeleteObject = async (req, res) => {
  try {
    const name = req.params.name;
    const steam_id = req.params.steam_id;
    //const message = req.params.message;
    const entityName = req.params.entityName;
    console.log(entityName);
    var iconv = new Iconv("UTF-8", "CP1251");
    //var result_string = iconv.convert(message);
    var entity = iconv.convert(entityName);
    //console.log(entity.toString());
    var encodeName = iconv.convert(name);
    encodeName = encodeName.toString().replace(/\[|]/g, "");
    // console.log(
    //   `${chat_channel[channel].prefix}[${name}]: ${result_string.toString()}\n${
    //     chat_channel[channel].suffix
    //   }`
    // );
    // let start_str = { prefix: "```json", suffix: "```" };
    const body = {
      content: `${
        chat_channel[0].prefix
      }Куратор [${encodeName.toString()}(${steam_id})]: удалил ${entity.toString()}${
        chat_channel[0].suffix
      }`,
    };
    sendInDiscord(config.get("zeusLog"), body);
    // console.log(JSON.stringify(result_string.toString("utf-8")));
    res.send(`['ok']`);
  } catch (e) {
    console.log(e);
  }
};
