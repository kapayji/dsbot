import cp from "child_process";
import config from "config";
import { sendInDiscord } from "../../utils/discord/index.js";

// export let restartServer = async (req, res) => {
//   try {
//     cp.execFile("kill_test.cmd", {
//       cwd: "F:\\SteamLibrary\\steamapps\\common\\Arma 3\\",
//     });
//     console.log(req.params.name);
//   } catch (e) {
//     console.log(e.message);
//   }
// };
export let restartServer = async (req, res) => {
  try {
    let body = {};
    try {
      cp.execSync(
        "start C:\\Arma_Server\\kill_test.cmd"
      );
    } catch (e) {
      console.log(e);
    }
    try {
      cp.execSync(
        "start C:\\Arma_Server\\kill_test.cmd"
      );
    } catch (e) {
      console.log(e);
    }
    try {
      cp.execSync(
        "start C:\\Arma_Server\\kill_test.cmd"
      );
    } catch (e) {
      console.log(e);
    }
    // cp.execFile("kill_test.cmd", [`${req.params.port}`], {
    //   cwd: "C:\\Arma_Server\\",
    // });
    // console.log("1");
    body = {
      embeds: [
        {
          title: "System Info",
          color: "14177041",
          fields: [
            {
              name: "Restart",
              value: `Запущена процедура обновления`,
            },
          ],

          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: new Date(Date.now()),
        },
      ],
    };
    await sendInDiscord(config.get("restartLog"), body);
    try {
      cp.execSync(
        "C:\\Users\\k2r4m\\AppData\\Local\\Programs\\Python\\Python310\\python.exe C:\\Arma_Server\\a3modupdater.py"
      );
    } catch (e) {
      console.log(e);
    }
    body = {
      embeds: [
        {
          title: "System Info",
          color: "14177041",
          fields: [
            {
              name: "Restart",
              value: `Обновление завершено`,
            },
          ],

          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: new Date(Date.now()),
        },
      ],
    };
    sendInDiscord(config.get("restartLog"), body);
    cp.execFile("startserver.bat", [`${req.params.port}`], {
      cwd: "C:\\Arma_Server\\",
    });
    body = {
      embeds: [
        {
          title: "System Info",
          color: "14177041",
          fields: [
            {
              name: "Restart",
              value: `Ответ сервера: получена команда перезапуска сервера с портом ${req.params.port}`,
            },
          ],

          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: new Date(Date.now()),
        },
      ],
    };
    await sendInDiscord(config.get("restartLog"), body);
    for (let i = 0; i <= 2; i++) {
      setTimeout(
        async function (i) {
          cp.execFile(
            `starthc.bat`,
            [`${req.params.port}`],

            {
              cwd: "C:\\Arma_Server\\",
            }
          );
          body = {
            embeds: [
              {
                title: "System Info",
                color: "14177041",
                fields: [
                  {
                    name: "Restart",
                    value: `Ответ сервера: запуск HC экземпляр ${i + 1}`,
                  },
                ],

                thumbnail: {
                  url: "https://discdev.ru/images/6.png",
                },
                timestamp: new Date(Date.now()),
              },
            ],
          };
          await sendInDiscord(config.get("restartLog"), body);
        },
        (i + 1) * 60000,
        i
      );
    }
    body = {
      embeds: [
        {
          title: "System Info",
          color: "14177041",
          fields: [
            {
              name: "Restart",
              value: `Сервер запущен, спасибо что вы с нами, приятной игры!`,
            },
          ],

          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: new Date(Date.now()),
        },
      ],
    };
    await sendInDiscord(config.get("announce"), body);
    res.json({
      message: `Ok`,
    });
  } catch (e) {
    console.log(e.message);
  }
};
