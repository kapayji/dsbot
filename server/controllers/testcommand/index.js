import Player from "../../models/players/index.js";
import fetch from "node-fetch";
import iconv from "iconv";
import { sendInDiscord } from "../../utils/discord/index.js";
import config from "config";
const { Iconv } = iconv;

export let testCommand = async (req, res) => {
  try {
    const name = req.params.name;
    const side = req.params.side;
    const position_x = req.params.position_x;
    const position_y = req.params.position_y;
    const altitude = req.params.altitude;
    const direction = req.params.direction;
    const steam_id = req.params.steam_id;
    const loadout = req.params.loadout;
    const player_exist = await Player.findOne({ steam_id });
    if (player_exist) {
      console.log(JSON.stringify(player_exist));
      // let temp = [];
      // temp.push(JSON.stringify(player_exist));
      return res.send(
        `['${player_exist.name}','${player_exist.side}','${player_exist.steam_id}','${player_exist.loadout}']`
      );
    }
    const player = new Player({
      name,
      side,
      position_x,
      position_y,
      altitude,
      direction,
      steam_id,
      loadout,
    });
    await player.save();
    return res.send(
      `['${player.name}','${player.side}','${player.steam_id}','${player.loadout}']`
    );
  } catch (e) {
    console.log(e);
  }
};

export let sendEmbed = async (req, res) => {
  try {
    const message = req.params.message;
    const name = req.params.name;
    const position = req.params.position;
    const direction = req.params.direction;
    const steam_id = req.params.steam_id;
    const player_in_db = await Player.findOne({ steam_id });
    var iconv = new Iconv("UTF-8", "CP1251");
    var result_string = iconv.convert(message);
    console.log(result_string.toString());
    let start_str = { prefix: "```json", suffix: "```" };
    const body = {
      embeds: [
        {
          title: "Arma 3 LFP Staff bot!",
          description:
            start_str.prefix +
            `\n${result_string.toString()} от игрока "${name}", позиция ${position}, направление ${direction}, steamID: ${steam_id}.
Информация из БД:
Игрок с именем "${
              player_in_db.name
            }" найден в БД (имя взято из БД по steamID)` +
            start_str.suffix,
          // description: start_str.prefix + '\n"123"' + start_str.suffix,
          thumbnail: {
            url: "https://i.yapx.ru/Mw7aR.gif",
          },
          // image: {
          //   url: "https://i.pinimg.com/originals/ca/27/92/ca2792dbfb93b4c04c503faeee8d966e.gif",
          // },
        },
      ],
    };
    await fetch(
      "https://discordapp.com/api/v9/channels/838709705772892161/messages",
      {
        method: "POST",
        headers: {
          "Content-Disposition": 'form-data; name="payload_json"',
          "Content-Type": "application/json",
          Authorization:
            "Bot OTAyODQwMTYyMTkzNjM3NDQ3.YXkRbA.jbbt-nltGNZxmSxDJrePhte3_f4",
        },
        body: JSON.stringify(body),
      }
    );
    console.log(JSON.stringify(result_string.toString("utf-8")));
    res.send(`['ok']`);
  } catch (e) {
    console.log(e);
  }
};
export let sentTestString = async (req, res) => {
  try {
    const message = req.params.message;
    var iconv = new Iconv("UTF-8", "CP1251");
    var result_string = iconv.convert(message);
    const body = {
      content: `${result_string}`,
    };
    sendInDiscord(config.get("zeusLog"), body);
    res.send("['ok']");
  } catch (e) {
    console.log(e);
  }
};
