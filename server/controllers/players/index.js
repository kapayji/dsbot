import { sendInDiscord } from "../../utils/discord/index.js";
import config from "config";
import Player from "../../models/players/index.js";
import iconv from "iconv";
import Ban from "../../models/ban/index.js";
const { Iconv } = iconv;

const chat_channel = [
  { prefix: "```\n", suffix: "```" },
  { prefix: "```ini\n", suffix: "```" },
  { prefix: "```fix\n", suffix: "```" },
  { prefix: "```diff\n+", suffix: "```" },
  { prefix: "```css\n", suffix: "```" },
  { prefix: "```\n", suffix: "```" },
];

export let banGetToPlayer = async (req, res) => {
  try {
    const steam_id = req.params.steam_id;
    const reason = req.params.reason;
    const initial = req.params.initial;
    let body = {};
    let date = new Date(Date.now());
    console.log(`${date}`);
    const ban_exits = await Ban.findOne({ steam_id });
    if (ban_exits) {
      body = {
        embeds: [
          {
            title: "System ban Info",
            color: "14177041",
            fields: [
              {
                name: "Info",
                value: `Запись о бане UID(${steam_id}) уже есть в БД`,
              },
              {
                name: "Информация из БД",
                value: `UID: ${ban_exits.steam_id}\nДата внесеня записи: ${ban_exits.start_date}\nИнициатор: ${ban_exits.initial}\nПричина: ${ban_exits.reason}`,
              },
            ],

            thumbnail: {
              url: "https://discdev.ru/images/6.png",
            },
            timestamp: date,
          },
        ],
      };
      sendInDiscord(config.get("banList"), body);
      return res.status(400).send("ok");
    }
    const new_ban = new Ban({
      steam_id,
      reason,
      initial,
    });

    await new_ban.save();
    body = {
      embeds: [
        {
          title: "System ban Info",
          color: "14177041",
          fields: [
            {
              name: "Info",
              value: `Запись о бане игрока с UID(${steam_id}) создана в БД\nИнициатор: ${initial}`,
            },
            {
              name: "Информация из БД",
              value: `UID: ${new_ban.steam_id}\nДата внесеня записи: ${new_ban.start_date}\nИнициатор: ${new_ban.initial}\nПричина: ${new_ban.reason}`,
            },
          ],
          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: date,
        },
      ],
    };
    sendInDiscord(config.get("banList"), body);
    res.send("ok");
  } catch (e) {
    console.log(e);
  }
};

export let banRemoveFromPlayer = async (req, res) => {
  try {
    const steam_id = req.params.steam_id;
    const initial = req.params.initial;
    const ban_exits = await Ban.findOne({ steam_id });
    let body;
    let date = new Date(Date.now());
    console.log(date);
    if (!ban_exits) {
      body = {
        embeds: [
          {
            title: "System ban Info",
            color: "14177041",
            description: `Запись о бане игрока с UID(${steam_id}) не найдена в БД`,
            thumbnail: {
              url: "https://discdev.ru/images/6.png",
            },
            timestamp: date,
          },
        ],
      };
      sendInDiscord(config.get("banList"), body);
      return res.status(400).json({ message: "Зевс не найден в БД" });
    }
    await Ban.findOneAndDelete(ban_exits);

    body = {
      embeds: [
        {
          title: "System ban Info",
          color: "14177041",
          description: `Запись о бане UID(${steam_id}) удалена из БД\nИнициатор: ${initial}`,
          thumbnail: {
            url: "https://discdev.ru/images/6.png",
          },
          timestamp: date,
        },
      ],
    };
    sendInDiscord(config.get("banList"), body);
    res.json({ message: `Бан снят` });
  } catch (e) {
    console.log(e);
  }
};

export let playerConnect = async (req, res) => {
  try {
    const message = req.params.message;
    const steam_id = req.params.steam_id;
    var iconv = new Iconv("UTF-8", "CP1251");
    var conv_message = iconv.convert(message);
    console.log(conv_message.toString(), steam_id);
    const body = {
      content: `${conv_message.toString()}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    return res.send(`['${steam_id}']`);
  } catch (e) {
    console.log(e);
  }
};

export let playerConnected = async (req, res) => {
  try {
    const name = req.params.name;
    const steam_id = req.params.steam_id;
    var str_name = name.replace(/\[|]/g, "");
    //console.log(steam_id);
    let body = {};
    body = {
      content: `${chat_channel[4].prefix}Игрок [${str_name}(${steam_id})] подключился к серверу${chat_channel[2].suffix}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    const player_exist = await Player.findOne({ name });
    if (player_exist) {
      //console.log(
      //`['${player_exist.steam_id}','${player_exist.position}','${player_exist.loadout}']`
      //);
      body = {
        content: `${chat_channel[4].prefix}Игрок [${str_name}(${steam_id})] найден в БД, начата загрузка данных игрока${chat_channel[2].suffix}`,
      };
      sendInDiscord(config.get("systemLog"), body);
      // let temp = [];
      // temp.push(JSON.stringify(player_exist));
      return res.send(
        `['${player_exist.steam_id}','${player_exist.zeus}','${player_exist.position}','${player_exist.loadout}']`
      );
    }
    body = {
      content: `${chat_channel[4].prefix}Игрок [${str_name}(${steam_id})] не найден в БД, начата процедура создания игрока и сохранение информации в БД${chat_channel[2].suffix}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    const player = new Player({
      name,
      steam_id,
    });
    await player.save();
    body = {
      content: `${chat_channel[4].prefix}Игрок [${str_name}(${steam_id})] создан и сохранён в БД${chat_channel[2].suffix}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    return res.send(
      `['${player.steam_id}','${player.zeus}','${player.position}','${player.loadout}']`
    );
  } catch (e) {
    console.log(e);
  }
};

export let playerDisconnected = async (req, res) => {
  try {
    const name = req.params.name;
    const side = req.params.side;
    const position = req.params.position;
    const direction = req.params.direction;
    const loadout = req.params.loadout;
    const steam_id = req.params.steam_id;
    var str_name = name.replace(/\[|]/g, "");
    let body = {};
    body = {
      content: `${chat_channel[4].prefix}Игрок [${str_name}(${steam_id})] отсоединился${chat_channel[2].suffix}`,
    };
    sendInDiscord(config.get("systemLog"), body);

    const player = await Player.findOne({ name });
    body = {
      content: `${chat_channel[4].prefix}Начата процедура сохранения данных игрока [${str_name}(${steam_id})]${chat_channel[4].suffix}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    player.side = side;
    player.position = position;
    player.direction = direction;
    player.loadout = loadout;
    await player.save();
    body = {
      content: `${chat_channel[4].prefix}Данные игрока [${str_name}(${steam_id})] сохранены${chat_channel[4].suffix}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    return res.send(`['Игрок сохранён']`);
  } catch (e) {
    console.log(e);
  }
};

export let playerMessage = async (req, res) => {
  try {
    const name = req.params.name;
    const steam_id = req.params.steam_id;
    const message = req.params.message;
    var iconv = new Iconv("UTF-8", "CP1251");
    var conv_message = iconv.convert(message);
    conv_message = conv_message.toString().replace(/\[|]/g, "");
    var str_name = name.replace(/\[|]/g, "");
    const body = {
      content: `${chat_channel[4].prefix}[${str_name}(${steam_id})] ${conv_message}${chat_channel[2].suffix}`,
    };
    console.log(str_name.toString());
    sendInDiscord(config.get("playersLog"), body);
    res.send(`['ok']`);
  } catch (e) {
    console.log(e);
  }
};

export let playerRespawn = async (req, res) => {
  try {
    const name = req.params.name;
    const steam_id = req.params.steam_id;
    var str_name = name.replace(/\[|]/g, "");
    //console.log(steam_id);
    let body = {};
    body = {
      content: `${chat_channel[4].prefix}Игрок [${str_name}(${steam_id})] возродился${chat_channel[2].suffix}`,
    };
    sendInDiscord(config.get("systemLog"), body);
    const player = await Player.findOne({ name });
    return res.send(
      `['${player.steam_id}','${player.zeus}','${player.position}','${player.loadout}']`
    );
  } catch (e) {
    console.log(e);
  }
};
