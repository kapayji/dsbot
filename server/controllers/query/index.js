import mysql from "mysql2/promise";
import config from "config";

const connection = await mysql.createConnection({
  host: config.get("host"),
  user: config.get("user"),
  password: config.get("password"),
  database: config.get("database"),
});

export async function getPlayerInBlackList(req, res) {
  try {
    const [rows] = await connection.execute(
      "SELECT * FROM `blacklist` WHERE `uid`=?",
      [req.params.uid]
    );
    res.json(rows);
  } catch (e) {
    res.status(500).json({ message: "Error getAllFunc" });
    console.log(e);
  }
}

export async function setPlayerInBlackList(req, res) {
  try {
    const [rows] = await connection.execute(
      "SELECT `name` FROM `players` WHERE `pid`=?",
      [req.params.uid]
    );
    const [banRow] = await connection.execute(
      "SELECT `uid` FROM `blacklist` WHERE `uid`=?",
      [req.params.uid]
    );
    console.log(req.params.initialName);
    if (rows.length) {
      if (!banRow.length) {
        await connection.execute(
          "INSERT INTO `blacklist` (`uid`,`reason`, `delay`,`name`,`initialName`) VALUES (?,?,?,?,?)",
          [
            req.params.uid,
            req.params.reason,
            req.params.delay,
            rows[0].name,
            req.params.initialName,
          ]
        );
        res.json({
          message: `Игрок ${req.params.uid} забанен на сервере`,
          name: rows[0].name,
          reason: req.params.reason,
          delay: req.params.delay,
        });
      } else {
        res.json({
          message: `Такой ${req.params.uid} уже в таблице банов`,
          name: banRow[0].name,
          reason: banRow[0].reason,
          delay: banRow[0].delay,
        });
      }
    } else {
      res.status(401).json({
        message:
          "Игрок с таким uid в БД не найден, если вы уверены, что всё верно - используйте команду '$forceban'",
      });
    }
  } catch (e) {
    res.status(500).json({ message: "Error getAllFunc" });
    console.log(e);
  }
}
export async function forceSetPlayerInBlackList(req, res) {
  try {
    await connection.execute(
      "INSERT INTO `blacklist` (`uid`,`reason`, `delay`,`name`,`initialName`) VALUES (?,?,?,?,?)",
      [
        req.params.uid,
        req.params.reason,
        req.params.delay,
        "n/a",
        req.params.initialName,
      ]
    );
    res.json({
      message: `Игрок ${req.params.uid} забанен на сервере`,
      name: "n/a",
      reason: req.params.reason,
      delay: req.params.delay,
    });
  } catch (e) {
    res.status(500).json({ message: "Error getAllFunc" });
    console.log(e);
  }
}
export async function deletePlayerFromBanList(req, res) {
  try {
    const [banRow] = await connection.execute(
      "SELECT * FROM `blacklist` WHERE `uid`=?",
      [req.params.uid]
    );
    if (banRow.length) {
      await connection.execute("DELETE FROM `blacklist` WHERE `uid`=?", [
        req.params.uid,
      ]);
      res.json({
        message: `${req.params.uid} удалён из таблицы банов`,
        name: banRow[0].name,
        reason: req.params.reason,
      });
    } else {
      res.json({
        message: `${req.params.uid} не найден в таблице банов`,
      });
    }
  } catch (e) {
    res.status(500).json({ message: "Error getAllFunc" });
    console.log(e);
  }
}
export async function addPlayerInZeusTable(req, res) {
  try {
    const [zeusRows] = await connection.execute(
      "SELECT `uid` FROM `zeuslist` WHERE `uid`=?",
      [req.params.uid]
    );
    if (zeusRows.length === 0) {
      const [rows] = await connection.execute(
        "SELECT `name` FROM `players` WHERE `pid`=?",
        [req.params.uid]
      );
      // console.log(
      //   req.params.uid,
      //   rows[0].name,
      //   req.params.ingamerole,
      //   req.params.initialName
      // );
      if (rows.length) {
        await connection.execute(
          "INSERT INTO `zeuslist` (`uid`,`name`, `ingamerole`,`initialName`) VALUES (?,?,?,?)",
          [
            req.params.uid,
            rows[0].name,
            req.params.ingamerole,
            req.params.initialName,
          ]
        );
        res.json({
          message: `Игроку ${req.params.uid} выдан зевс`,
          name: rows[0].name,
          ingamerole: req.params.ingamerole,
          initialName: req.params.initialName,
        });
      } else {
        res.status(401).json({
          message: `Игрок ${req.params.uid} не найден в БД, проверьте и повторите попытку`,
        });
      }
    } else {
      res.status(401).json({
        message: `Игрок ${req.params.uid} уже в таблице зевсов`,
      });
    }
  } catch (e) {
    console.log(e);
  }
}
export async function deletePlayerFromZeusTable(req, res) {
  try {
    const [zeusRows] = await connection.execute(
      "SELECT * FROM `zeuslist` WHERE `uid`=?",
      [req.params.uid]
    );
    let name = zeusRows.length ? zeusRows[0].name : "";
    if (zeusRows.length !== 0) {
      await connection.execute("DELETE FROM `zeuslist` WHERE `uid`=?", [
        req.params.uid,
      ]);
      res.json({
        message: `С игрока ${req.params.uid} снят зевс`,
        name: name,
        initialName: req.params.initialName,
      });
    } else {
      res.status(401).json({
        message: `Игрок ${req.params.uid} не найден в БД, проверьте и повторите попытку`,
      });
    }
  } catch (e) {
    console.log(e);
  }
}
