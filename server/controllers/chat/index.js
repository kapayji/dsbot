import { sendInDiscord } from "../../utils/discord/index.js";
import config from "config";
import Player from "../../models/players/index.js";
import iconv from "iconv";
const { Iconv } = iconv;

export let chatMessage = async (req, res) => {
  try {
    const message = req.params.message;
    const name = req.params.name;
    const channel = req.params.channel;
    //console.log(123);
    const chat_channel = [
      { prefix: "```\n", suffix: "```" },
      { prefix: "```ini\n", suffix: "```" },
      { prefix: "```fix\n", suffix: "```" },
      { prefix: "```diff\n+", suffix: "```" },
      { prefix: "```css\n", suffix: "```" },
      { prefix: "```\n", suffix: "```" },
    ];
    var iconv = new Iconv("UTF-8", "CP1251");
    var result_string = iconv.convert(message);
    let new_name = name.replace(/\[|]/g, "");
    const body = {
      content: `${
        channel == 16 ? chat_channel[4].prefix : chat_channel[channel].prefix
      }[${new_name}]: ${result_string.toString()}\n${
        channel == 16 ? chat_channel[4].suffix : chat_channel[channel].suffix
      }`,
    };
    sendInDiscord(config.get("chatLog"), body);
    res.send(`['ok']`);
  } catch (e) {
    console.log(e);
  }
};
