import Messages from "../../models/messages/index.js";

export let getMessages = async (req, res) => {
  try {
    console.log(req.params.user);
    const author = req.params.user;
    const messages = await Messages.find({ author });
    if (!messages.length) {
      return res.status(400).json({ message: "Not found" });
    }
    let mesArr = [];
    messages.forEach((v) => {
      mesArr.push({ author: v.author, content: v.content });
    });
    console.log(mesArr);
    res.json(mesArr);
  } catch (e) {
    console.log(e);
  }
};
export let setMessage = async (req, res) => {
  try {
    console.log(req.params.user);
    const { content } = req.body;
    const message = new Messages({ content, author: req.params.user });
    await message.save();
    res.json({ message: "Message saved in DB" });
  } catch (e) {
    console.log(e);
  }
};
