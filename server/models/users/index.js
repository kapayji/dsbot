import mongoose from "mongoose";
const User = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  createDate: { type: String, default: new Date(Date.now()).toLocaleString() },
  expiringDate: {
    type: String,
    default: new Date(1000 * 60 * 60 * 24 * 31 + Date.now()).toLocaleString(),
  },
});
export default mongoose.model("User", User);
