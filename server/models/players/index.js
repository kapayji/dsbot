import mongoose from "mongoose";
const Player = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  side: { type: String, default: "" },
  position: { type: String, default: "[0,0,0]" },
  direction: { type: String, default: "0" },
  steam_id: { type: String, required: true },
  loadout: { type: String, default: "[]" },
  zeus: { type: String, default: "0" },
  owner: { type: mongoose.ObjectId, ref: "User" },
});

export default mongoose.model("Player", Player);
