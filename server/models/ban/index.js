import mongoose from "mongoose";
const Ban = new mongoose.Schema({
  steam_id: { type: String, required: true, unique: true },
  reason: { type: String, default: "uebok" },
  start_date: {
    type: String,
    default: new Date(Date.now()).toLocaleString(),
  },
  initial: { type: String, required: true },
});

export default mongoose.model("Ban", Ban);
