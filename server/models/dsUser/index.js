import mongoose from "mongoose";
const dsUser = new mongoose.Schema({
  guild_id: { type: String, required: true },
  user_id: { type: String, required: true, unique: true },
  roles: [
    {
      id: { type: String, required: true },
      startDate: { type: String, default: new Date(Date.now()) },
      expiringDate: {
        type: String,
        default: new Date(1000 * 60 * 60 * 24 * 30 + Date.now()),
      },
    },
  ],
});
export default mongoose.model("dsUser", dsUser);
