import mongoose from "mongoose";
const Zeus = new mongoose.Schema({
  steam_id: { type: String, required: true, unique: true },
  role: { type: String, default: "Ивентолог" },
  start_date: {
    type: String,
    default: new Date(Date.now()).toLocaleString(),
  },
  initial: { type: String, required: true },
});

export default mongoose.model("Zeus", Zeus);
