import Router from "express";
import { handleMessage } from "../../controllers/systemmessage/index.js";

export const systemmessageRouter = Router();

systemmessageRouter.get("/handlemessage/:message", async (req, res) => {
  handleMessage(req, res);
});
