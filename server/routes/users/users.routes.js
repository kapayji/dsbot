import Router from "express";
import validator from "express-validator";
const { check } = validator;
import { registrationUser } from "../../controllers/users/index.js";

export const userRoutes = Router();

userRoutes.post(
  "/registration",
  [
    check("email", "Incorrect email").isEmail(),
    check("password", "Minimal 6 characters").isLength({ min: 6 }),
  ],
  async (req, res) => {
    registrationUser(req, res);
  }
);
