import Router from "express";
import {
  curatorWatchingCreateGroup,
  curatorWatchingCreateObject,
  curatorWatchingDeleteObject,
  zeusGetToPlayer,
  zeusRemoveFromPlayer,
} from "../../controllers/zeus/index.js";

export const zeusRouter = Router();
zeusRouter.get(
  "/group/:name/:steam_id/:entityName/:position/:count",
  async (req, res) => {
    curatorWatchingCreateGroup(req, res);
  }
);
zeusRouter.get(
  "/object/:name/:steam_id/:entityName/:position",
  async (req, res) => {
    curatorWatchingCreateObject(req, res);
  }
);
zeusRouter.get("/delete/:name/:steam_id/:entityName", async (req, res) => {
  curatorWatchingDeleteObject(req, res);
});
zeusRouter.get("/gettoplayer/:steam_id/:role/:initial", async (req, res) => {
  zeusGetToPlayer(req, res);
});
zeusRouter.get("/removefromplayer/:steam_id/:initial", async (req, res) => {
  zeusRemoveFromPlayer(req, res);
});
