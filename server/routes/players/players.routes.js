import Router from "express";
import {
  banGetToPlayer,
  banRemoveFromPlayer,
  playerConnect,
  playerConnected,
  playerDisconnected,
  playerMessage,
  playerRespawn,
} from "../../controllers/players/index.js";

export const playersRouter = Router();

playersRouter.get("/playerconnect/:message/:steam_id", async (req, res) => {
  playerConnect(req, res);
});
playersRouter.get("/playerconnected/:name/:steam_id", async (req, res) => {
  playerConnected(req, res);
});
playersRouter.get(
  "/playerdisconnected/:name/:side/:position/:loadout/:direction/:steam_id",
  async (req, res) => {
    playerDisconnected(req, res);
  }
);
playersRouter.get("/playerrespawn/:name/:steam_id", async (req, res) => {
  playerRespawn(req, res);
});
playersRouter.get(
  "/playermessage/:name/:steam_id/:message",
  async (req, res) => {
    playerMessage(req, res);
  }
);
playersRouter.get("/ban/:steam_id/:reason/:initial", async (req, res) => {
  banGetToPlayer(req, res);
});
playersRouter.get("/removeban/:steam_id/:initial", async (req, res) => {
  banRemoveFromPlayer(req, res);
});
