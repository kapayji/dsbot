import Router from "express";
import {
  getPlayerInBlackList,
  setPlayerInBlackList,
  forceSetPlayerInBlackList,
  deletePlayerFromBanList,
  addPlayerInZeusTable,
  deletePlayerFromZeusTable,
} from "../../controllers/query/index.js";

export const queryRouter = Router();

queryRouter.get("/:uid", async (req, res) => {
  getPlayerInBlackList(req, res);
});
queryRouter.get(
  "/create/ban/:uid/:reason/:delay/:initialName",
  async (req, res) => {
    setPlayerInBlackList(req, res);
  }
);
queryRouter.get(
  "/create/forceban/:uid/:reason/:delay/:initialName",
  async (req, res) => {
    forceSetPlayerInBlackList(req, res);
  }
);
queryRouter.get("/create/deleteban/:uid/:reason", async (req, res) => {
  // console.log(123);
  deletePlayerFromBanList(req, res);
});
queryRouter.get(
  "/create/addzeus/:uid/:ingamerole/:initialName",
  async (req, res) => {
    addPlayerInZeusTable(req, res);
  }
);
queryRouter.get("/create/delzeus/:uid/:initialName", async (req, res) => {
  deletePlayerFromZeusTable(req, res);
});
