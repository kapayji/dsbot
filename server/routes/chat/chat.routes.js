import Router from "express";
import { chatMessage } from "../../controllers/chat/index.js";

export const chatRouter = Router();

chatRouter.get("/chat/:message/:name/:channel", async (req, res) => {
  chatMessage(req, res);
});
