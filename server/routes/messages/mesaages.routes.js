import Router from "express";
import { getMessages, setMessage } from "../../controllers/messages/index.js";

export const messagesRouter = Router();

messagesRouter.get("/:user", async (req, res) => {
  getMessages(req, res);
});
messagesRouter.post("/create/:user", async (req, res) => {
  setMessage(req, res);
});
