import {
  sendEmbed,
  sentTestString,
  testCommand,
} from "../../controllers/testcommand/index.js";

import Router from "express";

export const testRouter = Router();

testRouter.get(
  "/test/:name/:side/:position_x/:position_y/:altitude/:direction/:steam_id/:loadout",
  async (req, res) => {
    testCommand(req, res);
  }
);

testRouter.get(
  "/discord/embed/:message/:name/:position/:direction/:steam_id",
  async (req, res) => {
    sendEmbed(req, res);
  }
);
testRouter.get("/teststring/:message", async (req, res) => {
  sentTestString(req, res);
});
