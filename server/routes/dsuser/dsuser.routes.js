import Router from "express";
import {
  addSubscribeToUser,
  getUserRoles,
} from "../../controllers/dsUser/index.js";

export const dsUserRoutes = Router();

dsUserRoutes.get(
  "/addrole/:guild_id/:user_id/:role_id/:delay",
  async (req, res) => {
    addSubscribeToUser(req, res);
  }
);
dsUserRoutes.get("/getroles/:guild_id/:user_id", async (req, res) => {
  getUserRoles(req, res);
});
// dsUserRoutes.get("/delrole/:guild_id/:user_id/:role_id", async (req, res) => {
//   addSubscribeToUser(req, res);
// });
