import Discord from "discord.js";
import fs from "fs";
import fetch from "node-fetch";

export let run = async (client, message, args, prefix) => {
  await fetch(
    "http://localhost:5000/api/mes/create/" + message.author.username,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ content: args.join(" ") }, null, 2),
    }
  )
    .then((response) => response.json())
    .then((data) => {
      // console.log(client);
      try {
        const exampleEmbed = new Discord.MessageEmbed()
          .setColor("#43e2f7")
          .setTitle("from DB")
          .setAuthor(message.guild.name)
          .setDescription(data.message)
          .setTimestamp()
          .setFooter("Bot, 2021");
        message.channel.send(exampleEmbed);
      } catch (e) {
        console.log(e.message);
      }
    });
};
export let help = { name: "db" };
