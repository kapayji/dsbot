import Discord from "discord.js";
import fs from "fs";
import fetch from "node-fetch";
import config from "config";

export let run = async (client, message, args, prefix) => {
  let checked = 0;
  config
    .get("checkArr")
    .forEach((v) => (message.member.roles.cache.has(v) ? (checked = 1) : v));
  let initialName =
    message.member.user.username + " " + message.member.user.discriminator;
  if (
    (checked === 1 && args.length === 3) ||
    (checked === 1 && args.length === 2)
  ) {
    if (args[0] === "add") {
      const uid = args[1];
      const ingamerole = args[2] ? args[2] : "zeus";
      fetch(
        "http://localhost:5000/api/zeus/gettoplayer/" +
          `${uid}/` +
          `${ingamerole}/` +
          `${initialName}`
      );
      // if (!response.ok) {
      //   const data = await response.json();
      //   try {
      //     const exampleEmbed = new Discord.MessageEmbed()
      //       .setColor("#43e2f7")
      //       .setTitle("Zeus info")
      //       .setAuthor("System")
      //       .setDescription(`Сообщение: ${data.message}`)
      //       .setTimestamp()
      //       .setFooter("by KapayJI, 2021");
      //     message.channel.send(exampleEmbed);
      //   } catch (e) {
      //     console.log(e.message);
      //   }
      // } else {
      //   const data = await response.json();
      //   try {
      //     const exampleEmbed = new Discord.MessageEmbed()
      //       .setColor("#43e2f7")
      //       .setTitle("Zeus info")
      //       .setAuthor("System")
      //       .setDescription(
      //         `Сообщение: ${data.message}
      //       Имя на сервере: ${data.name}
      //       Игровая роль: ${data.ingamerole}
      //       Выдал: ${initialName}`
      //       )
      //       .setTimestamp()
      //       .setFooter("by KapayJI, 2021");
      //     message.channel.send(exampleEmbed);
      //   } catch (e) {
      //     console.log(e.message);
      //   }
      // }
    }
    if (args[0] === "del") {
      const uid = args[1];
      fetch(
        "http://localhost:5000/api/zeus/removefromplayer/" +
          `${uid}/` +
          `${initialName}`
      );
      // .then((response) => response.json())
      // .then((data) => {
      //   try {
      //     const exampleEmbed = new Discord.MessageEmbed()
      //       .setColor("#43e2f7")
      //       .setTitle("Zeus info")
      //       .setAuthor("System")
      //       .setDescription(
      //         `Сообщение: ${data.message}
      //     Имя на сервере: ${data.name}
      //     Выдал: ${initialName}`
      //       )
      //       .setTimestamp()
      //       .setFooter("by KapayJI, 2021");
      //     message.channel.send(exampleEmbed);
      //   } catch (e) {
      //     console.log(e.message);
      //   }
      // });
    }
  } else {
    try {
      const exampleEmbed = new Discord.MessageEmbed()
        .setColor("#43e2f7")
        .setTitle("Zeus info")
        .setAuthor("System")
        .setDescription(
          `Сообщение: У вас недостаточно прав для выполнения этой команды или неверное число аргументов
          Выдать зевса: $zeus [add] [steam id] [ingame role]
          Снять зевс: $zeus [del] [steam id]`
        )
        .setTimestamp()
        .setFooter("by KapayJI, 2021");
      message.channel.send(exampleEmbed);
    } catch (e) {
      console.log(e);
    }
  }
  await message.delete(message);
};
export let help = { name: "zeus" };
