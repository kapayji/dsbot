import Discord from "discord.js";
import fs from "fs";
import fetch from "node-fetch";
import config from "config";

export let run = async (client, message, args, prefix) => {
  // console.log(args);
  try {
    if (message.channel.id === "752945592467980339") {
      let checked = 0;
      config
        .get("restartRights")
        .forEach((v) =>
          message.member.roles.cache.has(v) ? (checked = 1) : v
        );
      if (checked === 1) {
        if (args.length > 1 || args.length === 0) {
          try {
            const exampleEmbed = new Discord.MessageEmbed()
              .setColor("#43e2f7")
              .setTitle("System info")
              .setAuthor(message.guild.name)
              .setDescription("Недопустимое число аргументов")
              .setTimestamp()
              .setFooter("Bot, 2021");
            message.channel.send(exampleEmbed);
            await message.delete();
          } catch (e) {
            console.log(e.message);
          }
        } else {
          await fetch(
            "http://localhost:5000/api/restart/restartserver/" + args[0]
          );
          // .then((response) => response.json())
          // .then((data) => {
          //   try {
          //     const exampleEmbed = new Discord.MessageEmbed()
          //       .setColor("#43e2f7")
          //       .setTitle("from DB")
          //       .setAuthor(message.guild.name)
          //       .setDescription(data.message)
          //       .setTimestamp()
          //       .setFooter("Bot, 2021");
          //     message.channel.send(exampleEmbed);
          //   } catch (e) {
          //     console.log(e.message);
          //   }
          // });
          await message.delete();
        }
      } else {
        try {
          const exampleEmbed = new Discord.MessageEmbed()
            .setColor("#43e2f7")
            .setTitle("System info")
            .setAuthor(message.guild.name)
            .setDescription("Недостаточно прав")
            .setTimestamp()
            .setFooter("Bot, 2021");
          message.channel.send(exampleEmbed);
          await message.delete();
        } catch (e) {
          console.log(e.message);
        }
      }
    }
    await message.delete();
  } catch (e) {
    console.log(e);
  }
};
export let help = { name: "restart" };
