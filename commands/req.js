import Discord from "discord.js";
import fs from "fs";
import fetch from "node-fetch";

export let run = async (client, message, args, prefix) => {
  await fetch("http://localhost:5000/api/mes/" + message.author.username)
    .then((response) => response.json())
    .then((data) => {
      let tempStr;
      !data.message
        ? data.forEach((v) => {
            tempStr = tempStr + `${v.content}\n`;
          })
        : data.message;
      try {
        const exampleEmbed = new Discord.MessageEmbed()
          .setColor("#43e2f7")
          .setTitle("from DB")
          .setAuthor(message.guild.name)
          .setDescription(
            data.message || `Author:${message.author.username}\n${tempStr}`
          )
          .setTimestamp()
          .setFooter("Bot, 2021");
        message.channel.send(exampleEmbed);
      } catch (e) {
        console.log(e.message);
      }
    });
};
export let help = { name: "req" };
