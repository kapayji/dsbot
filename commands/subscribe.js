import Discord from "discord.js";
import fs from "fs";
import fetch from "node-fetch";

export let run = async (client, message, args, prefix) => {
  try {
    // console.log(args);
    const user_id = args[0];
    const role_id = args[1];
    const delay = args[2] || 30;
    console.log(`${message.guild.id}/${user_id}/${role_id}/${delay}`);
    await fetch(
      `http://localhost:5000/api/dsuser/addrole/${message.guild.id}/${user_id}/${role_id}/${delay}`
    );
  } catch (e) {
    console.log(e);
  }
};
export let help = { name: "subscribe" };
