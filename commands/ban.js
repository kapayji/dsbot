import Discord from "discord.js";
import fs from "fs";
import fetch from "node-fetch";
import config from "config";

export let run = async (client, message, args, prefix) => {
  let checked = 0;
  config
    .get("checkArr")
    .forEach((v) => (message.member.roles.cache.has(v) ? (checked = 1) : v));
  let initialName =
    message.member.user.username + " " + message.member.user.discriminator;
  if (checked === 1) {
    const uid = args[0];
    const reason = args[1] !== undefined ? args[1] : "Причина не указана";
    const delay = args[2] !== undefined ? args[2] : 5;
    await fetch(
      "http://localhost:5000/api/players/ban/" +
        `${uid}/` +
        `${reason}/` +
        `${initialName}`
    );
    // .then((response) => response.json())
    // .then((data) => {
    //   try {
    //     const exampleEmbed = new Discord.MessageEmbed()
    //       .setColor("#43e2f7")
    //       .setTitle("Ban info")
    //       .setAuthor("System")
    //       .setDescription(
    //         `Сообщение: ${data.message}
    //       Имя на сервере: ${data.name ? data.name : ""}
    //       Причина: ${data.reason ? data.reason : ""}
    //       Длительность: ${
    //         data.delay ? data.delay + " чего-то, ещё не придумал" : ""
    //       }
    //       Инициатор: ${initialName}`
    //       )
    //       .setTimestamp()
    //       .setFooter("by KapayJI, 2021");
    //     message.channel.send(exampleEmbed);
    //   } catch (e) {
    //     console.log(e.message);
    //   }
    // });
    await message.delete(message);
  } else {
    try {
      const exampleEmbed = new Discord.MessageEmbed()
        .setColor("#43e2f7")
        .setTitle("Ban info")
        .setAuthor("System")
        .setDescription(
          "Сообщение: У вас недостаточно прав для выполнения этой команды"
        )
        .setTimestamp()
        .setFooter("by KapayJI, 2021");
      message.channel.send(exampleEmbed);
      await message.delete(message);
    } catch (e) {
      console.log(e);
    }
  }
};
export let help = { name: "ban" };
